package com.jasperreport;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;

public class JasperReportCompile {
    public static void main(String[] args) {
        String sourceFileName = "C:\\Users\\" + System.getProperty("user.name") + "\\IdeaProjects\\JasperReportSpringBoot\\src\\main\\resources\\jasper_report_template.jrxml";
        System.out.println("Compiling Report Design ...");
        try {
            /**
             * Compile the report to a file name same as
             * the JRXML file name
             */
            JasperCompileManager.compileReportToFile(sourceFileName);
        } catch (JRException e) {
            e.printStackTrace();
        }
        System.out.println("Done compiling!!! ...");
    }
}