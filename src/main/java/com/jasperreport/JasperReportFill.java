package com.jasperreport;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JasperReportFill {
    public static void main(String[] args) {
        String sourceFileName = "C:\\Users\\zarkas\\IdeaProjects\\BankFilesGradle\\jasper_report_template.jasper";

        BufferedInputStream bufferedInputStream = null;

        try {
            bufferedInputStream = new BufferedInputStream( new FileInputStream(sourceFileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String printFileName = null;
        DataBeanList DataBeanList = new DataBeanList();
        ArrayList<DataBean> dataList = DataBeanList.getDataBeanList();
        JRBeanCollectionDataSource beanColDataSource =
                new JRBeanCollectionDataSource(dataList);

        Map parameters = new HashMap();
        try {
            printFileName = JasperFillManager.fillReportToFile(sourceFileName,
                    parameters, beanColDataSource);
            if (printFileName != null) {
                /**
                 * 1- export to PDF
                 */
//                JasperExportManager.exportReportToPdfFile(printFileName,"C://sample_report.pdf");

                /**
                 * 2- export to HTML
                 */
                JasperExportManager.exportReportToHtmlFile(printFileName,"C://sample_report.html");

                /**
                 * 3- export to Excel sheet
                 */
                JRXlsxExporter exporterXlsx = new JRXlsxExporter();

                // Set input and output ...
                SimpleXlsxReportConfiguration reportConfig = new SimpleXlsxReportConfiguration();
                reportConfig.setSheetNames(new String[] { "Employee Data" });

                exporterXlsx.setExporterInput(new SimpleExporterInput(printFileName));
                exporterXlsx.setExporterOutput(new SimpleOutputStreamExporterOutput("C://sample_report.xlsx"));

                exporterXlsx.setConfiguration(reportConfig);
                exporterXlsx.exportReport();

                /**
                 * 4- export to Excel sheet
                 */
                JRCsvExporter exporterCsv = new JRCsvExporter();

                // Set input and output
                exporterCsv.setExporterInput(new SimpleExporterInput(printFileName));
                exporterCsv.setExporterOutput(
                        new SimpleWriterExporterOutput("C://sample_report.csv"));

                exporterCsv.exportReport();
            }
        } catch (JRException e) {
            e.printStackTrace();
        }
    }
}