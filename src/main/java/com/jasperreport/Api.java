package com.jasperreport;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableScheduling
public class Api {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcome() {
        return "Welcome";
    }

    //    @Scheduled(cron = "0/20 * * * * ?") //Schedule every 20 seconds
    @Scheduled(cron = "0 29 11 ? * *") //Specify time -> Seconds Minutes Hours ? * *
    public void generateFiles() {
        JasperReportFill.main(null);

        System.out.println("Files generated successfully...");
    }
}
