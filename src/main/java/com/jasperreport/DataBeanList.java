package com.jasperreport;

import java.util.ArrayList;

public class DataBeanList {
    public ArrayList<DataBean> getDataBeanList() {
        ArrayList<DataBean> dataBeanList = new ArrayList<DataBean>();

        dataBeanList.add(produce("Babis", "Greece"));
        dataBeanList.add(produce("Yiannis", "Italy"));
        dataBeanList.add(produce("George", "France"));
        dataBeanList.add(produce("Bill", "Germany"));

        return dataBeanList;
    }

    /**
     * This method returns a DataBean object,
     * with name and country set in it.
     */
    private DataBean produce(String name, String country) {
        DataBean dataBean = new DataBean();
        dataBean.setName(name);
        dataBean.setCountry(country);

        return dataBean;
    }
}